﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using HttpApi.Dto;
using RestSharp;
using static HttpApi.Dto.SendRequest;

namespace Sender
{

    class Program
    {

        private const string ApiAddress = "http://127.0.0.1:5000";
        private const string SmscPath = "api/Smsc/send";
        private static bool _canRun = true;

        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            var request = new SendRequest
            {
                Login = "LOGIN",
                Password = "PASSWORD",
                Send = new SendBody
                {
                    Message = new List<MessageBody>
                    {
                        new MessageBody {Id = "100500", Text = "TestText", Receiver = "+79204567878"},
                        new MessageBody {Id = "100502", Text = "TestText", Receiver = "+79204567878"},
                        new MessageBody {Id = "100501", Text = "TestText2", Receiver = "+79204567879", Sender = "TestSender"}
                    }
                }
            };

            Task.Run(() => Sender(request));

            Console.WriteLine("Press any key to stop sender");
            Console.ReadLine();
            _canRun = false;
            Console.ReadLine();
        }

        private static void Sender(SendRequest request)
        {
            while (_canRun)
            {
                var client = new RestClient(ApiAddress);
                var restRequest = new RestRequest(SmscPath, Method.POST);
                restRequest.AddHeader("Content-Type", "application/xml");

                var formatter = new XmlSerializer(typeof(SendRequest));
                string xml;
                var stringWriter = new Utf8StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    formatter.Serialize(writer, request);
                    xml = stringWriter.ToString();
                }

                Console.WriteLine($"xml = {xml}");
                restRequest.AddParameter("application/xml", xml, ParameterType.RequestBody);

                var result = client.Execute(restRequest);

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine($"Error = {result.ErrorMessage}");
                }
                else
                {
                    Console.WriteLine($"result status code = {result.StatusCode}");
                    Console.WriteLine(result.Content);
                }

                Thread.Sleep(5000);
            }
        }

        private sealed class Utf8StringWriter : StringWriter
        {

            public override Encoding Encoding => Encoding.UTF8;

        }

    }

}