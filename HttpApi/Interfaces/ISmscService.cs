using HttpApi.Dto;

namespace HttpApi.Interfaces
{

    public interface ISmscService
    {
        /// <summary>
        /// Validate and send
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SendResponse Send(SendRequest request);
    }

}