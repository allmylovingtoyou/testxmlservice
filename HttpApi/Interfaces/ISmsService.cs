using HttpApi.Dto;

namespace HttpApi.Interfaces
{

    public interface ISmsService
    {

        /// <summary>
        /// Send sms
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SendResponse Send(SendRequest request);

    }

}