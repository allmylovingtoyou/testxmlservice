﻿using HttpApi.Dto;
using HttpApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HttpApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SmscController : Controller
    {

        private readonly ISmscService _smscService;

        public SmscController(ISmscService smscService)
        {
            _smscService = smscService;
        }
        
        /// <summary>
        /// Send sms by xml
        /// </summary>
        /// <param name="request">SendRequest</param>
        /// <returns>SendResponse</returns>
        [HttpPost]
        public SendResponse Send([FromBody] SendRequest request)
        {
            return _smscService.Send(request);
        }
    }
}