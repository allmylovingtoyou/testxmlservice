using System.Linq;
using System.Net;
using HttpApi.Dto;
using HttpApi.Interfaces;

namespace HttpApi.Services
{

    /// <summary>
    /// Validate requests and call external service
    /// </summary>
    public class SmscService : ISmscService
    {

        private readonly ISmsService _service;

        public SmscService(ISmsService service)
        {
            _service = service;
        }

        private const string TestLogin = "LOGIN";
        private const string TestPassword = "PASSWORD";

        /// <inheritdoc />
        public SendResponse Send(SendRequest request)
        {
            var checkResult = Validate(request);
            if (checkResult != null)
            {
                return checkResult;
            }

            return _service.Send(request);
        }

        private SendResponse Validate(SendRequest request)
        {
            //Nulls
            if (request?.Send?.Message == null 
                || !request.Send.Message.Any() 
                || request.Login == null 
                || request.Password == null)
            {
                return CreateErrorResponse(HttpStatusCode.BadRequest.ToString());
            }

            //Trust auth
            if (!(request.Login.Equals(TestLogin) && request.Password.Equals(TestPassword)))
            {
                return CreateErrorResponse(HttpStatusCode.Unauthorized.ToString());
            }

            //Unique message id
            var isIdsNotUnique = request.Send.Message
                .Select(message => message.Id)
                .GroupBy(id => id)
                .Any(pair => pair.Count() > 1);
            if (isIdsNotUnique)
            {
                return CreateErrorResponse(HttpStatusCode.BadRequest.ToString());
            }

            //More any checks...

            return null;
        }

        private SendResponse CreateErrorResponse(string errorCode)
        {
            return new SendResponse
            {
                Error = new ErrorBody {Error = errorCode}
            };
        }

    }

}