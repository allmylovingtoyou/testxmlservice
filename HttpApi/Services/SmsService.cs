using System;
using System.Collections.Generic;
using System.Linq;
using HttpApi.Dto;
using HttpApi.Interfaces;

namespace HttpApi.Services
{

    /// <summary>
    /// Emulate external sms service
    /// </summary>
    public class SmsService : ISmsService
    {

        private readonly Random _random = new Random();

        /// <inheritdoc />
        public SendResponse Send(SendRequest request)
        {
            return new SendResponse
            {
                Send = new Send
                {
                    Message = ProcessMessages(request).ToList()
                }
            };
        }

        private IEnumerable<Message> ProcessMessages(SendRequest request)
        {
            return request.Send.Message
                .Select(message => new Message
                {
                    Id = message.Id,
                    ServerId = Guid.NewGuid().ToString(),
                    Text = message.Text,
                    MsgErrCode = GetRandomErrorCode()
                });
        }

        private string GetRandomErrorCode()
        {

            return _random.Next(0, 2) >= 1
                ? "Sample error code"
                : null;
        }

    }

}