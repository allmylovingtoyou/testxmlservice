﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HttpApi.Dto
{

    [XmlRoot(ElementName = "package")]
    public class SendRequest
    {
        [XmlAttribute(AttributeName = "login")]
        public string Login { get; set; }

        [XmlAttribute(AttributeName = "password")]
        public string Password { get; set; }

        [XmlElement(ElementName = "send")]
        public SendBody Send { get; set; }

        [XmlRoot(ElementName = "send")]
        public class SendBody
        {
            [XmlElement(ElementName = "message")]
            public List<MessageBody> Message { get; set; } = new List<MessageBody>();
        }

        [XmlRoot(ElementName = "message")]
        public class MessageBody
        {
            [XmlAttribute(AttributeName = "id")] 
            public string Id { get; set; }

            [XmlAttribute(AttributeName = "receiver")]
            public string Receiver { get; set; }

            [XmlAttribute(AttributeName = "sender")]
            public string Sender { get; set; }

            [XmlText] public string Text { get; set; }
        }
    }

}