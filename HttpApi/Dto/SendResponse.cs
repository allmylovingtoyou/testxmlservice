﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HttpApi.Dto
{

    [XmlRoot(ElementName = "package")]
    public class SendResponse
    {
        [XmlElement(ElementName = "error")]
        public ErrorBody Error { get; set; }
        
        [XmlElement(ElementName = "send")]
        public Send Send { get; set; }
    }

    [XmlRoot(ElementName = "error")]
    public class ErrorBody
    {
        [XmlElement(ElementName = "error")]
        public string Error { get; set; }
    }
    
    [XmlRoot(ElementName = "send")]
    public class Send
    {
        [XmlElement(ElementName = "message")]
        public List<Message> Message { get; set; }
    }

    [XmlRoot(ElementName = "message")]
    public class Message
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "server_id")]
        public string ServerId { get; set; }

        [XmlText] 
        public string Text { get; set; }

        [XmlAttribute(AttributeName = "msg_err_code")]
        public string MsgErrCode { get; set; }

    }

}