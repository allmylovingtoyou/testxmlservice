using System.Collections.Generic;
using System.Linq;
using System.Net;
using HttpApi.Dto;
using HttpApi.Interfaces;
using HttpApi.Services;
using Moq;
using Xunit;

namespace HttpApiTests.Services
{

    public class SmscServiceTests
    {

        private readonly SmscService _service;
        private readonly SendRequest _validRequest;

        public SmscServiceTests()
        {
            _service = new SmscService(new Mock<ISmsService>().Object);
            _validRequest = CreateValidSendRequest();
        }

        [Fact]
        public void TestValidationValid()
        {
            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Null(result.Error);
        }

        [Fact]
        public void TestValidationNull()
        {
            var result = _service.Send(null);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationEmpty()
        {
            var result = _service.Send(new SendRequest());

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationLoginNull()
        {
            _validRequest.Login = null;

            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationPassNull()
        {
            _validRequest.Password = null;

            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationSendNull()
        {
            _validRequest.Send = null;

            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationMessageNull()
        {
            _validRequest.Send.Message = null;

            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestValidationMessageEmpty()
        {
            _validRequest.Send.Message = new List<SendRequest.MessageBody>();

            var result = _service.Send(_validRequest);

            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest.ToString(), result.Error.Error);
        }

        [Fact]
        public void TestSend()
        {
            var mock = new Mock<ISmsService>();
            mock.Setup(x => x.Send(_validRequest))
                .Returns(new SendResponse
                {
                    Send = new Send {Message = new List<Message> {new Message {Id = "1", ServerId = "2", Text = "Text", MsgErrCode = "error"}}}
                });

            var service = new SmscService(mock.Object);

            var result = service.Send(_validRequest);

            Assert.NotNull(result?.Send);
            Assert.Null(result.Error);
            Assert.Single(result.Send.Message);
            var message = result.Send.Message.First();
            Assert.Equal("1", message.Id);
            Assert.Equal("2", message.ServerId);
            Assert.Equal("Text", message.Text);
            Assert.Equal("error", message.MsgErrCode);
        }

        private SendRequest CreateValidSendRequest()
        {
            return new SendRequest
            {
                Login = "LOGIN",
                Password = "PASSWORD",
                Send = new SendRequest.SendBody
                {
                    Message = new List<SendRequest.MessageBody>
                    {
                        new SendRequest.MessageBody {Id = "100500", Text = "TestText", Receiver = "+79204567878"}
                    }
                }
            };
        }

    }

}